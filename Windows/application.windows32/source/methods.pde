void swap(int[] t, int a, int b)
{
  int tmp;
  
  tmp = t[a];
  t[a] = t[b];
  t[b] = tmp;
}

void shuffle(int[] t)
{
  int pick;
  
  for (int i = 0; i < t.length; i++)
  {
    pick = (int)random(data_size);
    swap(t, i, pick);
  }
}

// =======================================================================================================================================================================

void show()
{
  float wide = width / (float)data_size;
  
  if (mode == 1)
  {
    stroke(255);
    strokeWeight(1);
  }
  else
    noStroke();
  for (int i = 0; i < tab.length; i++)
  {
    if (mode == 2)
    {
      stroke(0);
      strokeWeight(1);
    }
    if (mode == 1)
    {
      if (end)
        stroke(255, 255, 0);
      else if (i == indexA || i == indexB)
        stroke(0, 255, 0);
      else if (i == showPivot)
        stroke(255, 0, 0);
      else
        stroke(255);
    }
    else
    {
      if (end)
        fill(255, 255, 0);
      else if (i == indexA || i == indexB)
        fill(0, 255, 0);
      else if (i == showPivot)
        fill(255, 0, 0);
      else
        fill(255);
    }
  
    float data = (tab[i] / (float)data_size) * height;
    if (mode == 1)
      point(i * wide, height - data);
    else
      rect(i * wide, height - data, wide, data);
  }
}

void showHelp()
{
  String txt = "Right/Left arrows : Change visualization mode\n"
             + "Up/Down arrows : Add/Remove 1 iteration per frame (+ shift for 10)\n"
             + "[+] / [-] keys : Add/Remove 10 numbers in data list (+ shift for 100)\n"
             + "Number keys : Change sorting algorithm\n"
             + "Enter/Return : Start loop\n"
             + "Backspace : Reset";
  textSize(12);
  textAlign(CENTER, CENTER);
  fill(255);
  text(txt, width / 2, height / 2);
}

void reset(boolean randomize)
{
  showPivot = -1;
  compare = 0;
  swaps = 0;
  frames = 0;
  
  if (randomize)
  {
    tab = new int[data_size];
    for (int i = 1; i <= data_size; i++)
      tab[i-1] = i;
    shuffle(tab);
  }

  indexA = algo == 3 ? 1 : 0;
  indexB = 1;
  len = tab.length;
  startIndex = 0;
  endIndex = tab.length - 1;
  dir = 1;
  if (algo == 4)
  {
    quick.clear();
    quick.add(new PVector(0, endIndex));
    indexA = indexB = 0;
  }
}