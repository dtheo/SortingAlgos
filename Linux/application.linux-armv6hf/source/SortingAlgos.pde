interface Sorting
{
  void sort(int[] t);
}

boolean shift = false;
boolean help = false;

// =======================================================================================================================================================================

String names[] = new String[] {
  "Bubble sort",
  "Cocktail shaker sort",
  "Selection sort",
  "Insertion sort",
  "Quick sort",
  "Odd-Even sort"
};

Sorting algos[] = new Sorting[] {
  new Sorting() {void sort(int[] t) {bubbleSort(t);}},
  new Sorting() {void sort(int[] t) {cocktailSort(t);}},
  new Sorting() {void sort(int[] t) {selectionSort(t);}},
  new Sorting() {void sort(int[] t) {insertionSort(t);}},
  new Sorting() {void sort(int[] t) {quickSort(t);}},
  new Sorting() {void sort(int[] t) {oddEvenSort(t);}}
};

// =======================================================================================================================================================================

int data_size = 200;
int mode = 0;
int algo = 0;
int speed = 10;

// =======================================================================================================================================================================

int len = -1;
int indexA = -1;
int indexB = -1;
boolean end = false;
boolean swapped = false;

int startIndex;
int endIndex;
int dir;

ArrayList<PVector> quick = new ArrayList<PVector>();
int showPivot = -1;

// =======================================================================================================================================================================

long compare = 0;
long swaps = 0;
int frames = 0;

int[] tab;
int status = 0;

void setup()
{
  reset(true);
  textFont(createFont("Monaco", 32));
  surface.setResizable(true);
// Use size for window mode and fullscreen for... fullscreen.
  size(800, 600);
  //fullScreen();
}

void draw()
{
  background(0);
  fill(255);
  textSize(12);
  textAlign(LEFT, TOP);
  text(names[algo] + " - "
    + compare + " comparisons - "
    + swaps + " swaps - "
    + frames + " frames x"
    + speed
    + (end ? " - Finished!\n" : "\n")
    + data_size + " values", 10, 10);
  textAlign(RIGHT, TOP);
  text((int)frameRate + " FPS", width - 10, 10);
  
  if (status == 1 && !help)
  {
    for (int i = 0; i < speed && !end; i++)
      algos[algo].sort(tab);
    if (!end)
      frames++;
  }
  if (help)
    showHelp();
  else
    show();
}

void keyPressed()
{
  int mult = shift ? 10 : 1;
  
  if (key == CODED)
  {
    if (keyCode == LEFT)
      mode = mode > 0 ? mode-1 : 2;
    else if (keyCode == RIGHT)
      mode = mode < 2 ? mode+1 : 0;
    else if (keyCode == UP)
      speed += 1 * mult;
    else if (keyCode == DOWN && speed - 1 * mult > 0)
      speed -= 1 * mult;
    else if (keyCode == SHIFT)
      shift = true;
  }
  else if ((key == ENTER || key == RETURN) && status == 0)
    status++;
  else if (key == BACKSPACE && status == 1)
  {
    status = 0;
    end = false;
    reset(true);
  }
  else if ((key == '+' || key == '=') && status == 0)
  {
    data_size += 10 * mult;
    reset(true);
  }
  else if (key == '-' && status == 0 && data_size - 10 * mult > 0)
  {
    data_size -= 10 * mult;
    reset(true);
  }
  else if (key >= '0' && key - '0' < algos.length && status == 0)
  {
    algo = key - '0';
    reset(false);
  }
  else if (key == 'h')
    help = true;
}

void keyReleased()
{
  if (keyCode == SHIFT)
    shift = false;
  else if (key == 'h')
    help = false;
}