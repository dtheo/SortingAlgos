void bubbleSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexA = indexB;
    indexB++;
  }
  else
  {
    if (!swapped)
      end = true;
    len--;
    indexB = 1;
    indexA = 0;
    swapped = false;
  }
}

void oddEvenSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexB += 2;
    indexA = indexB - 1;
  }
  else
  {
    if (!swapped)
      end = true;
    indexB = (indexB % 2 == 0 ? 1 : 2);
    indexA = indexB - 1;
    swapped = false;
  }
}

void cocktailSort(int[] t)
{
  if ((dir > 0 && indexB <= endIndex) || (dir < 0 && indexA >= startIndex))
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexB += dir;
    indexA += dir;
  }
  else
  {
    if (!swapped)
      end = true;
    if (dir > 0)
    {
      endIndex--;
      indexB = endIndex;
      indexA = indexB - 1;
    }
    else
    {
      startIndex++;
      indexA = startIndex;
      indexB = indexA + 1;
    }
    dir = -dir;
    swapped = false;
  }
}

void selectionSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
      indexA = indexB;
    indexB++;
  }
  else
  {
    if (indexA != startIndex)
    {
      swap(t, indexA, startIndex);
      swaps++;
    }
    startIndex++;
    indexA = startIndex;
    indexB = indexA+1;
    if (startIndex == len-1)
      end = true;
  }
}

void insertionSort(int[] t)
{
  compare++;
  if (indexB > 0 && t[indexB-1] > t[indexB])
  {
    swap(t, indexB-1, indexB);
    swaps++;
    indexB--;
  }
  else
  {
    indexA++;
    indexB = indexA;
    if (indexA == len)
      end = true;
  }
}

void quickSort(int[] t)
{
  if (quick.size() > 0)
  {
    int pivot = (int)(quick.get(0).y);
    int begin = (int)(quick.get(0).x);
    showPivot = pivot;
    if (indexB < pivot)
    {
      compare++;
      if (t[indexB] < t[pivot])
      {
        if (indexA != indexB)
        {
          swap(t, indexA, indexB);
          swaps++;
        }
        indexA++;
      }
      indexB++;
    }
    else
    {
      if (indexA != pivot)
      {
        swap(t, indexA, pivot);
        swaps++;
      }
      if (begin != indexA)
        quick.add(new PVector(begin, indexA - 1));
      if (indexA < pivot - 1)
        quick.add(new PVector(indexA + 1, pivot));
      quick.remove(0);
      if (quick.size() > 0)
        indexA = indexB = (int)(quick.get(0).x);
    }
  }
  else
    end = true;
}