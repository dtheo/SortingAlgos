import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SortingAlgos extends PApplet {

interface Sorting
{
  public void sort(int[] t);
}

boolean shift = false;
boolean help = false;

// =======================================================================================================================================================================

String names[] = new String[] {
  "Bubble sort",
  "Cocktail shaker sort",
  "Selection sort",
  "Insertion sort",
  "Quick sort",
  "Odd-Even sort"
};

Sorting algos[] = new Sorting[] {
  new Sorting() {public void sort(int[] t) {bubbleSort(t);}},
  new Sorting() {public void sort(int[] t) {cocktailSort(t);}},
  new Sorting() {public void sort(int[] t) {selectionSort(t);}},
  new Sorting() {public void sort(int[] t) {insertionSort(t);}},
  new Sorting() {public void sort(int[] t) {quickSort(t);}},
  new Sorting() {public void sort(int[] t) {oddEvenSort(t);}}
};

// =======================================================================================================================================================================

int data_size = 200;
int mode = 0;
int algo = 0;
int speed = 10;

// =======================================================================================================================================================================

int len = -1;
int indexA = -1;
int indexB = -1;
boolean end = false;
boolean swapped = false;

int startIndex;
int endIndex;
int dir;

ArrayList<PVector> quick = new ArrayList<PVector>();
int showPivot = -1;

// =======================================================================================================================================================================

long compare = 0;
long swaps = 0;
int frames = 0;

int[] tab;
int status = 0;

public void setup()
{
  reset(true);
  textFont(createFont("Monaco", 32));
  surface.setResizable(true);
// Use size for window mode and fullscreen for... fullscreen.
  
  //fullScreen();
}

public void draw()
{
  background(0);
  fill(255);
  textSize(12);
  textAlign(LEFT, TOP);
  text(names[algo] + " - "
    + compare + " comparisons - "
    + swaps + " swaps - "
    + frames + " frames x"
    + speed
    + (end ? " - Finished!\n" : "\n")
    + data_size + " values", 10, 10);
  textAlign(RIGHT, TOP);
  text((int)frameRate + " FPS", width - 10, 10);
  
  if (status == 1 && !help)
  {
    for (int i = 0; i < speed && !end; i++)
      algos[algo].sort(tab);
    if (!end)
      frames++;
  }
  if (help)
    showHelp();
  else
    show();
}

public void keyPressed()
{
  int mult = shift ? 10 : 1;
  
  if (key == CODED)
  {
    if (keyCode == LEFT)
      mode = mode > 0 ? mode-1 : 2;
    else if (keyCode == RIGHT)
      mode = mode < 2 ? mode+1 : 0;
    else if (keyCode == UP)
      speed += 1 * mult;
    else if (keyCode == DOWN && speed - 1 * mult > 0)
      speed -= 1 * mult;
    else if (keyCode == SHIFT)
      shift = true;
  }
  else if ((key == ENTER || key == RETURN) && status == 0)
    status++;
  else if (key == BACKSPACE && status == 1)
  {
    status = 0;
    end = false;
    reset(true);
  }
  else if ((key == '+' || key == '=') && status == 0)
  {
    data_size += 10 * mult;
    reset(true);
  }
  else if (key == '-' && status == 0 && data_size - 10 * mult > 0)
  {
    data_size -= 10 * mult;
    reset(true);
  }
  else if (key >= '0' && key - '0' < algos.length && status == 0)
  {
    algo = key - '0';
    reset(false);
  }
  else if (key == 'h')
    help = true;
}

public void keyReleased()
{
  if (keyCode == SHIFT)
    shift = false;
  else if (key == 'h')
    help = false;
}
public void bubbleSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexA = indexB;
    indexB++;
  }
  else
  {
    if (!swapped)
      end = true;
    len--;
    indexB = 1;
    indexA = 0;
    swapped = false;
  }
}

public void oddEvenSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexB += 2;
    indexA = indexB - 1;
  }
  else
  {
    if (!swapped)
      end = true;
    indexB = (indexB % 2 == 0 ? 1 : 2);
    indexA = indexB - 1;
    swapped = false;
  }
}

public void cocktailSort(int[] t)
{
  if ((dir > 0 && indexB <= endIndex) || (dir < 0 && indexA >= startIndex))
  {
    compare++;
    if (t[indexA] > t[indexB])
    {
      swap(t, indexA, indexB);
      swaps++;
      swapped = true;
    }
    indexB += dir;
    indexA += dir;
  }
  else
  {
    if (!swapped)
      end = true;
    if (dir > 0)
    {
      endIndex--;
      indexB = endIndex;
      indexA = indexB - 1;
    }
    else
    {
      startIndex++;
      indexA = startIndex;
      indexB = indexA + 1;
    }
    dir = -dir;
    swapped = false;
  }
}

public void selectionSort(int[] t)
{
  if (indexB < len)
  {
    compare++;
    if (t[indexA] > t[indexB])
      indexA = indexB;
    indexB++;
  }
  else
  {
    if (indexA != startIndex)
    {
      swap(t, indexA, startIndex);
      swaps++;
    }
    startIndex++;
    indexA = startIndex;
    indexB = indexA+1;
    if (startIndex == len-1)
      end = true;
  }
}

public void insertionSort(int[] t)
{
  compare++;
  if (indexB > 0 && t[indexB-1] > t[indexB])
  {
    swap(t, indexB-1, indexB);
    swaps++;
    indexB--;
  }
  else
  {
    indexA++;
    indexB = indexA;
    if (indexA == len)
      end = true;
  }
}

public void quickSort(int[] t)
{
  if (quick.size() > 0)
  {
    int pivot = (int)(quick.get(0).y);
    int begin = (int)(quick.get(0).x);
    showPivot = pivot;
    if (indexB < pivot)
    {
      compare++;
      if (t[indexB] < t[pivot])
      {
        if (indexA != indexB)
        {
          swap(t, indexA, indexB);
          swaps++;
        }
        indexA++;
      }
      indexB++;
    }
    else
    {
      if (indexA != pivot)
      {
        swap(t, indexA, pivot);
        swaps++;
      }
      if (begin != indexA)
        quick.add(new PVector(begin, indexA - 1));
      if (indexA < pivot - 1)
        quick.add(new PVector(indexA + 1, pivot));
      quick.remove(0);
      if (quick.size() > 0)
        indexA = indexB = (int)(quick.get(0).x);
    }
  }
  else
    end = true;
}
public void swap(int[] t, int a, int b)
{
  int tmp;
  
  tmp = t[a];
  t[a] = t[b];
  t[b] = tmp;
}

public void shuffle(int[] t)
{
  int pick;
  
  for (int i = 0; i < t.length; i++)
  {
    pick = (int)random(data_size);
    swap(t, i, pick);
  }
}

// =======================================================================================================================================================================

public void show()
{
  float wide = width / (float)data_size;
  
  if (mode == 1)
  {
    stroke(255);
    strokeWeight(1);
  }
  else
    noStroke();
  for (int i = 0; i < tab.length; i++)
  {
    if (mode == 2)
    {
      stroke(0);
      strokeWeight(1);
    }
    if (mode == 1)
    {
      if (end)
        stroke(255, 255, 0);
      else if (i == indexA || i == indexB)
        stroke(0, 255, 0);
      else if (i == showPivot)
        stroke(255, 0, 0);
      else
        stroke(255);
    }
    else
    {
      if (end)
        fill(255, 255, 0);
      else if (i == indexA || i == indexB)
        fill(0, 255, 0);
      else if (i == showPivot)
        fill(255, 0, 0);
      else
        fill(255);
    }
  
    float data = (tab[i] / (float)data_size) * height;
    if (mode == 1)
      point(i * wide, height - data);
    else
      rect(i * wide, height - data, wide, data);
  }
}

public void showHelp()
{
  String txt = "Right/Left arrows : Change visualization mode\n"
             + "Up/Down arrows : Add/Remove 1 iteration per frame (+ shift for 10)\n"
             + "[+] / [-] keys : Add/Remove 10 numbers in data list (+ shift for 100)\n"
             + "Number keys : Change sorting algorithm\n"
             + "Enter/Return : Start loop\n"
             + "Backspace : Reset";
  textSize(12);
  textAlign(CENTER, CENTER);
  fill(255);
  text(txt, width / 2, height / 2);
}

public void reset(boolean randomize)
{
  showPivot = -1;
  compare = 0;
  swaps = 0;
  frames = 0;
  
  if (randomize)
  {
    tab = new int[data_size];
    for (int i = 1; i <= data_size; i++)
      tab[i-1] = i;
    shuffle(tab);
  }

  indexA = algo == 3 ? 1 : 0;
  indexB = 1;
  len = tab.length;
  startIndex = 0;
  endIndex = tab.length - 1;
  dir = 1;
  if (algo == 4)
  {
    quick.clear();
    quick.add(new PVector(0, endIndex));
    indexA = indexB = 0;
  }
}
  public void settings() {  size(800, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SortingAlgos" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
