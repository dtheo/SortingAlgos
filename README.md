# SortingAlgos

![Cocktail Shaker Sort](https://puu.sh/Ccj6s/c55a537c63.png)
![Bubble Sort](https://puu.sh/Ccj6D/baaf781a7d.png)
![Selection Sort](https://puu.sh/Ccj76/bf3f4b8bea.png)
![Odd-Even Sort](https://puu.sh/Ccj7o/b408bcf381.png)
![Cocktail Shaker with dots](https://puu.sh/Ccj7C/f0544d598e.png)
![Quick sort](https://puu.sh/Ccj7N/b725391100.png)